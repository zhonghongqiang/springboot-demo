define('page/layout/default', ['require', 'exports', 'module', 'components/jquery/jquery', 'components/bootstrap/bootstrap', 'components/bootstrap/transition', "components/ace/js/ace-elements.min", "components/ace/js/ace.min", "static/js/common"], function(require, exports, module) {

  var $ = require('components/jquery/jquery');
  require('components/bootstrap/bootstrap');
  require('components/bootstrap/transition');
  require("components/ace/js/ace-elements.min");
  require("components/ace/js/ace.min");
  require("static/js/common");

});