/**
 * Created by zhong on 2016/11/6 0006.
 */
define('/static/page/order/confirm', ['require', 'exports', 'module'], function(require, exports, module) {

    jQuery(function($) {
        $('#datetimepicker').datetimepicker({
            format: 'yyyy-mm-dd'
        });
    });

});