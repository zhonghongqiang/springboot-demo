<%@ page contentType="text/html;charset=utf-8" %><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %><%@ taglib uri="/fis" prefix="fis"%><!DOCTYPE html>
<fis:html lang="en" framework="static/js/require.js">
	<fis:head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>橙子</title>

		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<fis:require name="components/bootstrap/css/bootstrap.css" />
		<fis:require name="components/font-awesome/css/font-awesome.css" />

		<!-- ace styles -->
		<fis:require name="components/ace/css/ace.css" />
		<!--[if lte IE 9]>
		<fis:require name="components/ace/css/ace-part2.min.css" prefix="<!--[if lte IE 9]>" affix="<![endif]-->"/>
		<fis:require name="components/ace/css/ace-ie.min.css" prefix="<!--[if lte IE 9]>" affix="<![endif]-->"/>
		<![endif]-->

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
		<!--[if lte IE 8]>
		<fis:require name="components/ace/js/html5shiv.min.js" prefix="<!--[if lte IE 8]>" affix="<![endif]-->" />
		<fis:require name="components/ace/js/respond.min.js" prefix="<!--[if lte IE 8]>" affix="<![endif]-->" />
		<![endif]-->

		<fis:require name="components/jquery/jquery.js" />
		<fis:require name="components/bootstrap/bootstrap.js" />

		<!-- ace scripts -->
		<fis:require name="components/ace/js/ace-elements.min.js" />
		<fis:require name="components/ace/js/ace.min.js" />
	</fis:head>
	<fis:body class="login-layout light-login">
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center">
								<h1>
									<span class="red">Aboat</span> <span class="grey" id="id-text2">脐橙订购系统</span>
								</h1>

							</div>
							<div class="space-6"></div>
							<div class="position-relative">
								<div id="login-box" class="login-box visible widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header blue lighter bigger">
												<i class="ace-icon fa fa-coffee green"></i> 用户登录
											</h4>
											<div class="space-6"></div>
											<form action="${ctx }/login" method="post">
												<fieldset>
													<label class="block clearfix"> <span class="block input-icon input-icon-right">
													<input type="email" class="form-control" placeholder="用户名" name="username" /> <i class="ace-icon fa fa-envelope"></i>
													</span>
													</label> <label class="block clearfix"> <span class="block input-icon input-icon-right"> <input
															type="password" class="form-control" placeholder="密码" name="password" /> <i class="ace-icon fa fa-lock"></i>
													</span>
													</label>

													<div class="space"></div>
													<div class="clearfix">
														<button type="button" class="width-35 pull-left btn btn-sm btn-primary" onclick="register()">
                                                            <i class="ace-icon fa fa-send"></i><span class="bigger-110">获取随机码</span>
                                                        </button>
														<button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
															<i class="ace-icon fa fa-key"></i> <span class="bigger-110">登录</span>
														</button>
													</div>
													<div class="space-4"></div>
												</fieldset>
											</form>
										</div>
									</div>

								</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<script type="text/javascript">
    function register(){
        var username = $("input[name='username']").val();
        if(username==""){
            alert("请输入您的邮箱地址");
            return;
        }
        $.ajax({
            url:'${ctx }/email/back',
            type:'POST',
            data:{"email":username},
            success:function(data){
                debugger;
                if(data.state){
                    alert(data.message);
                }else{
                    alert(data.message);
                }

            }
        });
    }
</script>
	</fis:body>
	<fis:script>require(['page/login']);</fis:script>
  <fis:require name="page/login.jsp" />
</fis:html>