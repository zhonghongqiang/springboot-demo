<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/fis" prefix="fis"%><!DOCTYPE html>
<fis:html lang="en" framework="static/js/require.js">
	<fis:head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>脐橙订购</title>

		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<fis:require name="components/bootstrap/css/bootstrap.css" />
		<fis:require name="components/font-awesome/css/font-awesome.css" />

		<!-- ace styles -->
		<fis:require name="components/ace/css/ace.css" />
		<!--[if lte IE 9]>
		<fis:require name="components/ace/css/ace-part2.min.css" prefix="<!--[if lte IE 9]>" affix="<![endif]-->"/>
		<fis:require name="components/ace/css/ace-ie.min.css" prefix="<!--[if lte IE 9]>" affix="<![endif]-->"/>
		<![endif]-->

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
		<!--[if lte IE 8]>
		<fis:require name="components/ace/js/html5shiv.min.js" prefix="<!--[if lte IE 8]>" affix="<![endif]-->" />
		<fis:require name="components/ace/js/respond.min.js" prefix="<!--[if lte IE 8]>" affix="<![endif]-->" />
		<![endif]-->

		<fis:require name="components/jquery/jquery.js" />
		<fis:require name="components/bootstrap/bootstrap.js" />

		<!-- ace scripts -->
		<fis:require name="components/ace/js/ace-elements.min.js" />
		<fis:require name="components/ace/js/ace.min.js" />
	</fis:head>
	<fis:body class="login-layout light-login">
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-12">
						<div class="center">
							<h2>订单详情页面</h2>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="col-md-12 column">
							<form
									<c:choose>
										<c:when test="${order.status == 4}">
											action="/order/recConfirm"
										</c:when>
										<c:otherwise>
											action="/order/payment"
										</c:otherwise>
									</c:choose>
									method="post" class="form-horizontal" role="form">
								<div class="form-group">
									<label class="col-sm-2 control-label">订单号：</label>
									<div class="col-sm-4">
										<p class="form-control-static">${order.id}</p>
									</div>
									<label class="col-sm-2 control-label">数量:</label>
									<div class="col-sm-4">
										<p class="form-control-static">${order.qty}<span>斤</span></p>
									</div>
								</div>
								<div style="display: none;">
									<input name="id" value="${order.id}">
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">订单状态：</label>
									<div class="col-sm-4">
										<p class="form-control-static" id="orderStatus">${order.statusName}</p>
									</div>
									<label class="col-sm-2 control-label">价格：</label>
									<div class="col-sm-4">
										<p class="form-control-static">${order.cost}<span>元</span></p>
									</div>
								</div>
								<div class="form-group">
									<label  class="col-sm-2 control-label">联系电话：</label>
									<div class="col-sm-4">
										<p class="form-control-static">${order.mobile}</p>
									</div>
									<label  class="col-sm-2 control-label">收货人：</label>
									<div class="col-sm-4">
										<p  class="form-control-static">${order.name}</p>
									</div>
								</div>
								<div class="form-group">
									<label  class="col-sm-2 control-label">收货地址：</label>
									<div class="col-sm-4">
										<p  class="form-control-static">${order.address}</p>
									</div>
									<label  class="col-sm-2 control-label">下单时间：</label>
									<div class="col-sm-4">
										<p  class="form-control-static"><fmt:formatDate value="${order.orderTime}" pattern="yyyy-MM-dd HH:mm:ss"/></p>
									</div>
								</div>
								<div class="form-group">
									<label  class="col-sm-2 control-label">留言：</label>
									<div class="col-sm-10">
										<p  class="form-control-static">${order.leaveMessage}</p>
									</div>
								</div>
								<c:if test="${order.status > 2 && order.status < 4}">
									<div class="form-group">
										<label  class="col-sm-2 control-label">预计发货日期：</label>
										<div class="col-sm-10">
											<p  class="form-control-static"><fmt:formatDate value="${order.deliveryDate}" pattern="yyyy-MM-dd HH:mm:ss"/></p>
										</div>
									</div>
								</c:if>
								<c:if test="${order.status > 3}">
									<div class="form-group">
										<label  class="col-sm-2 control-label">发货日期：</label>
										<div class="col-sm-10">
											<p  class="form-control-static"><fmt:formatDate value="${order.deliveryDate}" pattern="yyyy-MM-dd HH:mm:ss"/></p>
										</div>
									</div>
									<div class="form-group">
										<label  class="col-sm-2 control-label">快递公司：</label>
										<div class="col-sm-4">
											<p class="form-control-static">${order.logistics}</p>
										</div>
										<label  class="col-sm-2 control-label">运单号码：</label>
										<div class="col-sm-4">
											<p  class="form-control-static">${order.trackingNo}</p>
										</div>
									</div>
								</c:if>
								<c:if test="${order.status == 4}">
									<div class="form-group">
										<label  class="col-sm-2 control-label">确认收货留言：</label>
										<div class="col-sm-8">
											<textarea name="recConfirmMessage" class="form-control" rows="3">${order.recConfirmMessage}</textarea>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-offset-9 col-sm-2">
											<button type="submit" class="btn btn-default">确认收货</button>
										</div>
									</div>
								</c:if>
								<c:if test="${order.status == 5}">
									<div class="form-group">
										<label  class="col-sm-2 control-label">收货留言：</label>
										<div class="col-sm-10">
											<p  class="form-control-static">${order.recConfirmMessage}</p>
										</div>
									</div>
								</c:if>
								<c:if test="${empty order.status || order.status == 1}">
									<div class="form-group">
										<div class="col-sm-6 column">
											<img src="/static/img/1478401707266.jpg" class="img-responsive" />
										</div>
										<div class="col-sm-6 column">
											<p  class="form-control-static">
											<p>
												使用支付宝扫描左侧二维码支付，<span style="color: rgb(255, 0, 0);">并发送订单号。</span>支付成功请点击下发<span style="color: rgb(118, 146, 60);">【确认付款】</span>按钮。
											</p>
											</p>
											<div class="col-sm-offset-8 col-sm-4">
												<button type="submit" class="btn btn-default">确认付款</button>
											</div>
										</div>
									</div>
								</c:if>
							</form>
						</div>
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.main-content -->
		</div>
		<!-- /.main-container -->
		<!-- inline scripts related to this page -->
	</fis:body>
	<script type="text/javascript">
		$(function() {

		});
	</script>
	<fis:script>
		require(['page/login']);
	</fis:script>
	<%-- auto inject by fis3-preprocess-extlang--%>
	<fis:require name="page/login.jsp" />
</fis:html>