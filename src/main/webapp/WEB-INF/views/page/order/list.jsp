<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="/fis" prefix="fis"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fis:extends name="page/layout/default.jsp">

	<c:set var="pageTitle" value="订单管理" />
	<c:set var="select" value="false"/>

	<fis:block name="content">
	<div class="breadcrumbs" id="breadcrumbs">
		<ul class="breadcrumb">
			<li><i class="ace-icon fa fa-home home-icon"></i> <a href="${ctx }">首页</a></li>
		</ul>

		<div class="nav-search" id="nav-search">
			<form class="form-search">
				<span class="input-icon"> <input type="text" placeholder="Search ..." class="nav-search-input"
					id="nav-search-input" autocomplete="off" /> <i class="ace-icon fa fa-search nav-search-icon"></i>
				</span>
			</form>
		</div>
	</div>

	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<h3 class="header smaller lighter blue">订单列表</h3>
				<div class="col-xs-12" style="margin-bottom: 8px">
					<form name="form1" action="" method="get" class="form-horizontal" role="form">
						<div class="col-xs-4">
							<input type="text" class="form-control" autofocus="true" placeholder="邮箱/收货人名称/手机号" value="${order.keyword}" name="keyword">
						</div>
						<div class="col-xs-5">
							<c:forEach var="status" items="${statusList }">
								<c:forEach var="s" items="${order.statusList}">
									<c:if test="${status.dicKey eq s}">
										<c:set var="select" value="true"/>
									</c:if>
								</c:forEach>
								<c:choose>
									<c:when test="${select}">
										<label class="checkbox-inline">
											<input type="checkbox" name="statusList" checked="checked" value="${status.dicKey}">${status.dicValue}
										</label>
									</c:when>
									<c:otherwise>
										<label class="checkbox-inline">
											<input type="checkbox" name="statusList"  value="${status.dicKey}">${status.dicValue}
										</label>
									</c:otherwise>
								</c:choose>
								<c:set var="select" value="false"/>
							</c:forEach>
						</div>
						<div class="col-xs-3">
							<button class="fa btn btn-primary" type="submit" onclick="form1.action='${ctx }/order/list';form1.submit();">查询</button>
							<button class="fa btn btn-primary" type="submit" onclick="form1.action='${ctx }/order/export';form1.submit();">导出订单</button>
						</div>
					</form>
				</div>
				<div>
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>订单号</th>
								<th>注册邮箱</th>
								<th>收货人</th>
								<th>联系方式</th>
								<th>收货地址</th>
								<th>数量</th>
								<th>价格</th>
								<th>下单时间</th>
								<th>状态</th>
								<th>进度</th>
								<th>操作</th>
							</tr>
						</thead>

						<tbody>
							<c:if test="${orders == null}">
								<td rowspan="4" colspan="10">没有数据</td>
							</c:if>
							<c:forEach var="order" items="${orders }">
								<tr>
									<td>${order.id}</td>
									<td>${order.email}</td>
									<td>${order.name}</td>
									<td>${order.mobile}</td>
									<td>${order.address}</td>
									<td>${order.qty}&nbsp;斤</td>
									<td>${order.cost}</td>
									<td><fmt:formatDate value="${order.orderTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
									<td>${order.statusName}</td>
									<td>
										<div class="progress" style="margin-bottom:0px;">
											<div class="progress-bar
													<c:choose>
														<c:when test="${order.status > 3}">progress-bar-success</c:when>
														<c:when test="${order.status < 3}">progress-bar-info</c:when>
														<c:otherwise>progress-bar-warning</c:otherwise>
													</c:choose>"
												 role="progressbar" aria-valuenow="${order.status}" aria-valuemin="0" aria-valuemax="5" style="width: <fmt:formatNumber value="${order.status/5}" type="percent"/>;">
												<fmt:formatNumber value="${order.status/5}" type="percent"/>
											</div>
										</div>
									</td>
									<td>
										<div class="action-buttons">
											<c:if test="${order.status == 1}">
												<a class="green" href="${ctx }/order/edit?id=${order.id}"> <i class="ace-icon fa fa-pencil bigger-130">编辑</i></a>
												<a class="red" href="${ctx }/order/delete?id=${order.id}"> <i class="ace-icon fa fa-trash-o bigger-130">删除</i></a>
											</c:if>
											<c:if test="${order.status == 2}">
												<a class="yellow" href="${ctx }/order/confirm?id=${order.id}"> <i class="ace-icon fa fa-coffee bigger-130">确认订单</i></a>
											</c:if>
											<c:if test="${order.status == 3}">
												<a class="green" href="${ctx }/order/delivery?id=${order.id}"> <i class="ace-icon fa fa-plane bigger-130">订单发货</i></a>
											</c:if>
											<c:if test="${order.status == 4}">
												<a class="green" href="${ctx }/order/delivery?id=${order.id}"> <i class="ace-icon fa fa-plane bigger-130">重新发货</i></a>
												<a class="green" href="${ctx }/order/finish?id=${order.id}"> <i class="ace-icon fa fa-plane bigger-130">确认收货</i></a>
											</c:if>
											<a class="green" href="${ctx }/order/detail?id=${order.id}"> <i class="ace-icon fa fa-file-text bigger-130">详情</i></a>
										</div>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- /.page-content -->
	</fis:block>
 
  <%-- auto inject by fis3-preprocess-extlang--%>
  <fis:require name="page/order/list.jsp" />
</fis:extends>