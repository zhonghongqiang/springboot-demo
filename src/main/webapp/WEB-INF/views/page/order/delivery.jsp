<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="/fis" prefix="fis"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fis:extends name="page/layout/default.jsp">

	<c:set var="pageTitle" value="订单确认" />


	<fis:block name="content">
	<div class="breadcrumbs" id="breadcrumbs">
		<ul class="breadcrumb">
			<li><i class="ace-icon fa fa-home home-icon"></i> <a href="${ctx }/">首页</a></li>
		</ul>

		<div class="nav-search" id="nav-search">
			<form class="form-search">
				<span class="input-icon"> <input type="text" placeholder="Search ..." class="nav-search-input"
					id="nav-search-input" autocomplete="off" /> <i class="ace-icon fa fa-search nav-search-icon"></i>
				</span>
			</form>
		</div>
	</div>

	<div class="page-content">
		<div class="page-header">
			<h1>
				订单发货 <small> <i class="ace-icon fa fa-angle-double-right"></i> 发货订单信息
				</small>
			</h1>
		</div>
		<!-- /.page-header -->
		<div id="messageBox" class="error" style="display: none">输入有误，请先更正。</div>
		<div class="row">
			<div class="col-xs-12">
				<!-- PAGE CONTENT BEGINS -->
				<form id="inputForm" class="form-horizontal" role="form" action="${ctx }/order/delivery" method="post">
					<!-- #section:elements.form -->
					<c:if test="${!empty order.id}">
						<div class="form-group">
							<label class="col-sm-2 control-label">订单号：</label>
							<div class="col-sm-6">
								<p class="form-control-static">${order.id}</p>
							</div>
						</div>
					</c:if>
					<div class="form-group">
						<label class="col-sm-2 control-label">价格：</label>
						<div class="col-sm-6" style="color: orange;">
							<p class="form-control-static"><a id="orderCost" style="color: orange;">${order.cost}</a><span>元</span></p>
						</div>
					</div>
					<div style="display: none">
						<input type="text"  name="id" value="${order.id}">
						<input type="text"  name="cost" value="${order.cost}">
						<input type="text"  name="price" value="${order.price}">
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">订购数量：</label>
						<div class="col-sm-6">
							<select class="form-control col-sm-8" id="orderQty" name="qty" onchange="recountCost(this)">
								<c:forEach var="price" items="${prices }">
									<c:choose>
										<c:when test="${price.qty eq order.qty}">
											<option selected>${price.qty}</option>
										</c:when>
										<c:otherwise>
											<option>${price.qty}</option>
										</c:otherwise>
									</c:choose>
								</c:forEach>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label  class="col-sm-2 control-label">手机号码：</label>
						<div class="col-sm-6">
							<input type="text" class="form-control" value="${order.mobile}" name="mobile">
						</div>
					</div>
					<div class="form-group">
						<label  class="col-sm-2 control-label">收货人：</label>
						<div class="col-sm-6">
							<input type="text" name="name" value="${order.name}" class="form-control" >
						</div>
					</div>
					<div class="form-group">
						<label  class="col-sm-2 control-label">收货地址：</label>
						<div class="col-sm-6">
							<textarea name="address" class="form-control" rows="2">${order.address}</textarea>
						</div>
					</div>
					<div class="form-group">
						<label  class="col-sm-2 control-label">留言：</label>
						<div class="col-sm-6">
							<textarea name="leaveMessage" class="form-control" rows="3">${order.leaveMessage}</textarea>
						</div>
					</div>
					<div class="form-group">
						<label  class="col-sm-2 control-label">发货时间</label>
						<div class="col-sm-6">
							<input type="text" placeholder="yyyy-MM-dd" name="deliveryDate" value="<fmt:formatDate value="${order.deliveryDate}" pattern="yyyy-MM-dd"/>" class="form-control" >
						</div>
					</div>
					<div class="form-group">
						<label  class="col-sm-2 control-label">快递公司：</label>
						<div class="col-sm-6">
							<input type="text" name="logistics" value="${order.logistics}" class="form-control" >
						</div>
					</div>
					<div class="form-group">
						<label  class="col-sm-2 control-label">运单号：</label>
						<div class="col-sm-6">
							<input type="text" name="trackingNo" value="${order.trackingNo}" class="form-control" >
						</div>
					</div>
					<div class="clearfix form-actions">
						<div class="col-md-offset-7 col-md-1">
							<button class="btn btn-info" type="submit">
								<i class="ace-icon fa fa-check bigger-110"></i> 确认发货
							</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		//远程加载价格信息
		function recountCost(qty){
			qty = $(qty).val();
			$.ajax({
				url:'${ctx}/order/getCostByQty',
				type:'POST',
				data:{qty:qty},
				success:function(result){
					$("#orderCost").text(result['cost']);
					$("input[name='cost']").val(result['cost']);
					$("input[name='price']").val(result['price']);
				}
			});
		}
	</script>
	<!-- /.page-content -->
	</fis:block>
  <%-- auto inject by fis3-preprocess-extlang--%>
  <fis:require name="page/order/delivery.jsp" />
</fis:extends>