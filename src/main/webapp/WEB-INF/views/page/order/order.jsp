<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/fis" prefix="fis"%><!DOCTYPE html>
<fis:html lang="en" framework="static/js/require.js">
	<fis:head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>脐橙订购</title>

		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<fis:require name="components/bootstrap/css/bootstrap.css" />
		<fis:require name="components/font-awesome/css/font-awesome.css" />

		<!-- ace styles -->
		<fis:require name="components/ace/css/ace.css" />
		<!--[if lte IE 9]>
		<fis:require name="components/ace/css/ace-part2.min.css" prefix="<!--[if lte IE 9]>" affix="<![endif]-->"/>
		<fis:require name="components/ace/css/ace-ie.min.css" prefix="<!--[if lte IE 9]>" affix="<![endif]-->"/>
		<![endif]-->

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
		<!--[if lte IE 8]>
		<fis:require name="components/ace/js/html5shiv.min.js" prefix="<!--[if lte IE 8]>" affix="<![endif]-->" />
		<fis:require name="components/ace/js/respond.min.js" prefix="<!--[if lte IE 8]>" affix="<![endif]-->" />
		<![endif]-->

		<fis:require name="components/jquery/jquery.js" />
		<fis:require name="components/bootstrap/bootstrap.js" />

		<!-- ace scripts -->
		<fis:require name="components/ace/js/ace-elements.min.js" />
		<fis:require name="components/ace/js/ace.min.js" />
	</fis:head>
	<fis:body class="login-layout light-login">
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-12">
						<div class="center">
							<h4>脐橙订购系统</h4>
						</div>
					</div>

					<div class="col-sm-12">
						<div class="col-md-6 column">
							<img src="static/img/88f59fbe6ac85e71dc080f83e5260aad.jpg" class="img-responsive" />
						</div>
						<div class="col-md-6 column">
							<form action="${ctx}/order/add" method="post" class="form-horizontal" role="form">
								<c:if test="${!empty order.id}">
									<div class="form-group">
										<label class="col-sm-2 control-label">订单号：</label>
										<div class="col-sm-10">
											<p class="form-control-static">${order.id}</p>
										</div>
									</div>
								</c:if>
								<div class="form-group">
									<label class="col-sm-2 control-label">价格：</label>
									<div class="col-sm-10" style="color: orange;">
										<p class="form-control-static"><a id="orderCost" style="color: orange;"></a><span>元</span></p>
									</div>
								</div>
								<div style="display: none">
									<input type="text"  name="id" value="${order.id}">
									<input type="text"  name="cost" value="${order.cost}">
									<input type="text"  name="price" value="${order.price}">
								</div>
								<div class="form-group">
									<label class="col-sm-2 control-label">订购数量：</label>
									<div class="col-sm-10">
										<select class="form-control col-sm-8" id="orderQty" name="qty" onchange="recountCost(this.value)">
											<c:forEach var="price" items="${prices }">
												<c:choose>
													<c:when test="${price.qty eq order.qty}">
														<option selected>${price.qty}</option>
													</c:when>
													<c:otherwise>
														<option>${price.qty}</option>
													</c:otherwise>
												</c:choose>
											</c:forEach>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label  class="col-sm-2 control-label">手机号码：</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" value="${order.mobile}" name="mobile">
									</div>
								</div>
								<div class="form-group">
									<label  class="col-sm-2 control-label">收货人：</label>
									<div class="col-sm-10">
										<input type="text" name="name" value="${order.name}" class="form-control" >
									</div>
								</div>
								<div class="form-group">
									<label  class="col-sm-2 control-label">收货地址：</label>
									<div class="col-sm-10">
										<textarea name="address" class="form-control" rows="2">${order.address}</textarea>
									</div>
								</div>
								<div class="form-group">
									<label  class="col-sm-2 control-label">留言：</label>
									<div class="col-sm-10">
										<textarea name="leaveMessage" class="form-control" rows="3">${order.leaveMessage}</textarea>
									</div>
								</div>
								<div class="form-group">
									<div class="col-sm-offset-10 col-sm-2">
										<button type="submit" class="btn btn-default">提交订单</button>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="col-sm-12">
						<p>价格一览表</p>
						<div class="table-responsive">
							<table class="table  table-bordered">
								<thead>
									<tr>
										<c:forEach var="price" items="${prices }">
											<th>${price.qty}&nbsp;斤</th>
										</c:forEach>
										<th>更多...</th>
									</tr>
								</thead>
								<tbody>
									<c:if test="${prices != null}">
										<tr>
											<c:forEach var="price" items="${prices }">
												<td>${price.cost}&nbsp;元</td>
											</c:forEach>
											<td>肯定会更多...</td>
										</tr>
										<tr>
											<c:forEach var="price" items="${prices }">
												<td>${price.avg}&nbsp;元/斤</td>
											</c:forEach>
											<td>
												<div class="action-buttons">
													<a href="${ctx }/order/detail?id=${order.id}"> <i class="ace-icon fa fa-user  ">请联系我</i></a>
												</div>
											</td>
										</tr>
									</c:if>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-sm-12">
						<p>我的订单</p>
						<div class="table-responsive">
							<table class="table table-bordered">
								<thead>
								<tr>
									<th>订单号</th>
									<th>收货人</th>
									<th>收货地址</th>
									<th>数量</th>
									<th>价格</th>
									<th>订购时间</th>
									<th>状态</th>
									<th>操作</th>
								</tr>
								</thead>
								<tbody>
								<c:if test="${orders != null}">
									<c:forEach var="order" items="${orders }">
										<tr>
											<td>${order.id}</td>
											<td>${order.name}</td>
											<td>${order.address}</td>
											<td>${order.qty}&nbsp;斤</td>
											<td>${order.cost}</td>
											<td><fmt:formatDate value="${order.orderTime}" pattern="yyyy-MM-dd HH:mm:ss"/></td>
											<td>${order.statusName}</td>
											<td>
												<div class="action-buttons">
													<a href="${ctx }/order/detail?id=${order.id}"> <i class="ace-icon fa fa-file-text ">查看详情</i></a>
													<c:if test="${order.status == 1}">
														<a class="green" href="${ctx }/order?id=${order.id}"> <i class="ace-icon fa fa-pencil ">编辑订单</i></a>
													</c:if><c:if test="${order.status == 1}">
														<a class="green" href="${ctx }/order?id=${order.id}"> <i class="ace-icon fa fa-pencil ">编辑订单</i></a>
													</c:if>
												</div>
											</td>
										</tr>
									</c:forEach>
								</c:if>
								<c:if test="${orders == null}">
									<tr>
										<td rowspan="2" colspan="6">没有已创建的订单...</td>
									</tr>
								</c:if>
								</tbody>
							</table>
						</div>
					</div>
					<div class="col-sm-12">
						<div class="content">

						</div>
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->
			</div>
			<!-- /.main-content -->
		</div>
		<!-- /.main-container -->
	<!-- inline scripts related to this page -->
	</fis:body>
	<script type="text/javascript">
		function recountCost(qty){
			if(!qty){
				qty=$("#orderQty").val();
			}
			if(qty != undefined){
				$.ajax({
					url:'${ctx}/order/getCostByQty',
					type:'POST',
					data:{qty:qty},
					success:function(result){
						$("#orderCost").text(result['cost']);
						$("input[name='cost']").val(result['cost']);
						$("input[name='price']").val(result['price']);
					}
				});
			}
		}
		/**
		* 張開頁面第一次顯示
		 */
		recountCost();

	</script>
	<fis:script>
		require(['page/login']);
	</fis:script>

 
  <%-- auto inject by fis3-preprocess-extlang--%>
  <fis:require name="page/login.jsp" />
</fis:html>