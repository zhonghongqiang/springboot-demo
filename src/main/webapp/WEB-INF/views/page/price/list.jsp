<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="/fis" prefix="fis"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<fis:extends name="page/layout/default.jsp">

	<c:set var="pageTitle" value="价格管理" />

	<fis:block name="content">
	<div class="breadcrumbs" id="breadcrumbs">
		<ul class="breadcrumb">
			<li><i class="ace-icon fa fa-home home-icon"></i> <a href="${ctx }">首页</a></li>
		</ul>

		<div class="nav-search" id="nav-search">
			<form class="form-search">
				<span class="input-icon"> <input type="text" placeholder="Search ..." class="nav-search-input"
					id="nav-search-input" autocomplete="off" /> <i class="ace-icon fa fa-search nav-search-icon"></i>
				</span>
			</form>
		</div>
	</div>

	<div class="page-content">
		<div class="row">
			<div class="col-xs-12">
				<h3 class="header smaller lighter blue">价格列表</h3>
				<div class="clearfix">
					<div class="pull-left tableTools-container">
						<a class="btn btn-primary fa" href="${ctx }/price/create">新建价格数据</a>
						<a class="btn btn-primary fa" href="${ctx }/price/export" target="_blank">导出价格数据</a>
					</div>
				</div>

				<div>
					<table id="dynamic-table" class="table table-striped table-bordered table-hover">
						<thead>
							<tr>
								<th>编号</th>
								<th>数量</th>
								<th>价格</th>
								<th>单价</th>
								<th>备注</th>
								<th>操作</th>
							</tr>
						</thead>

						<tbody>
							<c:if test="${prices == null}">
								<td colspan="5">没有数据</td>
							</c:if>
							<c:forEach var="price" items="${prices }">
								<tr>
									<td>${price.id}</td>
									<td>${price.qty}&nbsp;斤</td>
									<td>${price.cost}&nbsp;元</td>
									<td>${price.avg}&nbsp;元/斤</td>
									<td>${price.remark}</td>
									<td>
										<div class="hidden-sm hidden-xs action-buttons">
											<a class="green" href="${ctx }/price/edit?id=${price.id}"> <i class="ace-icon fa fa-pencil bigger-130"></i></a>
											<a class="red" href="${ctx }/price/delete?id=${price.id}"> <i class="ace-icon fa fa-trash-o bigger-130"></i></a>
										</div>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!-- /.page-content -->
	</fis:block>
 
  <%-- auto inject by fis3-preprocess-extlang--%>
  <fis:require name="page/price/list.jsp" />
</fis:extends>