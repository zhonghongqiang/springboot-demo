<%@ page contentType="text/html;charset=utf-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/fis" prefix="fis"%>
<!DOCTYPE html>
<fis:html lang="en" framework="static/js/require.js">

	<c:set var="title" value="Aboat脐橙订购后台管理系统"/>

	<fis:head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title>${title}<c:if test="${pageTitle!=null}">  - ${pageTitle}</c:if></title>
		<meta name="description" content="overview &amp; stats" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
		<!-- bootstrap & fontawesome -->
		<fis:require name="components/bootstrap/css/bootstrap.css" />
		<fis:require name="components/font-awesome/css/font-awesome.css" />

		<!-- ace styles -->
		<fis:require name="components/ace/css/ace.css" />
		<!--[if lte IE 9]>
		<fis:require name="components/ace/css/ace-part2.min.css" prefix="<!--[if lte IE 9]>" affix="<![endif]-->"/>
		<fis:require name="components/ace/css/ace-ie.min.css" prefix="<!--[if lte IE 9]>" affix="<![endif]-->"/>
		<![endif]-->

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
		<!--[if lte IE 8]>
		<fis:require name="components/ace/js/html5shiv.min.js" prefix="<!--[if lte IE 8]>" affix="<![endif]-->" />
		<fis:require name="components/ace/js/respond.min.js" prefix="<!--[if lte IE 8]>" affix="<![endif]-->" />
		<![endif]-->

		<fis:block name="head"></fis:block>
	</fis:head>
	<fis:body class="no-skin">
		<fis:block name="header">
			<fis:widget name="widget/header/header.jsp" />
		</fis:block>

		<div class="main-container" id="main-container">
			<!-- #section:basics/sidebar -->
			<fis:block name="sidebar">
				<fis:widget name="widget/sidebarmenu/sidebarmenu.jsp" />
			</fis:block>
			<div class="main-content">
				<div class="main-content-inner">
					<fis:block name="content"></fis:block>
				</div>
			</div>
			<fis:block name="footer">
				<fis:widget name="widget/footer/footer.jsp" />
			</fis:block>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->
	</fis:body>
	<fis:script>
		require(['page/layout/default']);
	</fis:script>
 
  <%-- auto inject by fis3-preprocess-extlang--%>
  <fis:require name="page/layout/default.jsp" />
</fis:html>


