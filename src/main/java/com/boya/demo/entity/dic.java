package com.boya.demo.entity;

/**
 * Created by zhong on 2016/11/9 0009.
 */
public class Dic extends IdEntity {

    private Integer dicNo;
    private String dicKey;
    private String dicValue;

    public Integer getDicNo() {
        return dicNo;
    }

    public void setDicNo(Integer dicNo) {
        this.dicNo = dicNo;
    }

    public String getDicKey() {
        return dicKey;
    }

    public void setDicKey(String dicKey) {
        this.dicKey = dicKey;
    }

    public String getDicValue() {
        return dicValue;
    }

    public void setDicValue(String dicValue) {
        this.dicValue = dicValue;
    }
}
