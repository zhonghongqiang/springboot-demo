package com.boya.demo.entity;

import com.boya.demo.entity.IdEntity;

import java.util.Date;

/**
 * 臍橙訂單類
 * Created by zhong on 2016/11/5 0005.
 */
public class Orders extends IdEntity {

    private Long userId;
    private String email;
    private String name;
    private String mobile;
    private String leaveMessage;
    private String recConfirmMessage;
    private String address;
    private Date orderTime;
    private Integer status;
    private Integer[] statusList;
    private String keyword;
    private String statusName;
    private Date deliveryDate;
    private String logistics;
    private String trackingNo;
    private Double qty;
    private Double price;
    private Double cost;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLeaveMessage() {
        return leaveMessage;
    }

    public void setLeaveMessage(String leaveMessage) {
        this.leaveMessage = leaveMessage;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Date orderTime) {
        this.orderTime = orderTime;
    }

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getLogistics() {
        return logistics;
    }

    public void setLogistics(String logistics) {
        this.logistics = logistics;
    }

    public String getTrackingNo() {
        return trackingNo;
    }

    public void setTrackingNo(String trackingNo) {
        this.trackingNo = trackingNo;
    }

    public String getStatusName() {
        StringBuilder sb = new StringBuilder();
        if (status!=null){
            switch (status){
                case 1:sb.append("待付款");
                    break;
                case 2:sb.append("正在处理");
                    break;
                case 3:sb.append("正在备货");
                    break;
                case 4:sb.append("运输在途");
                    break;
                case 5:sb.append("已收货");
                    break;
                default : sb.append("你遇bug啦");
            }
        }else {
            sb.append("");
        }
        return sb.toString();

    }

    public String getRecConfirmMessage() {
        return recConfirmMessage;
    }

    public void setRecConfirmMessage(String recConfirmMessage) {
        this.recConfirmMessage = recConfirmMessage;
    }

    public Integer[] getStatusList() {
        return statusList;
    }

    public void setStatusList(Integer[] statusList) {
        this.statusList = statusList;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
