package com.boya.demo.entity;

/**
 * Created by zhong on 2016/11/6 0006.
 */
public class Price extends IdEntity {

    private Double qty;
    private Double cost;
    private Double avg;
    private String remark;

    public Double getQty() {
        return qty;
    }

    public void setQty(Double qty) {
        this.qty = qty;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Double getAvg() {
        if (cost!=null&&qty!=null&&qty!=0){
            return cost/qty;
        }
        return avg;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
