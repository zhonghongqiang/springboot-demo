package com.boya.demo.controlloer;

import com.boya.demo.dao.PriceDao;
import com.boya.demo.entity.Price;
import com.boya.demo.utils.DateUtils;
import com.boya.demo.utils.ExcelUtil;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * 价格控制类
 * Created by zhong on 2016/11/6 0006.
 */
@Controller
@RequestMapping("price")
public class PriceController extends BaseController{
    
    @Autowired
    PriceDao priceDao;
    
    private static final String path = "/page/price/";
    /**
     * 展示价格列表
     * @param model
     * @return
     */
    @RequestMapping(value = "list", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("prices",priceDao.getAll());
        return path+"list";
    }

    @RequestMapping("/create")
    public String create(Model model) {
        model.addAttribute("price", new Price());
        return path+"/edit";
    }

    @RequestMapping("/edit")
    public String edit(Long id, Model model){
        model.addAttribute("price", priceDao.getById(id));
        return path+"/edit";
    }

    @RequestMapping(value = "/export",method = RequestMethod.GET)
    public void finish(Model model,HttpServletResponse response){
        List<Price> priceList = priceDao.getAll();
        LinkedHashMap<String, String> propertyHeaderMap = new LinkedHashMap<>();
        propertyHeaderMap.put("id", "订单号");
        propertyHeaderMap.put("qty", "数量");
        propertyHeaderMap.put("cost", "价格");
        propertyHeaderMap.put("avg", "单价");
        propertyHeaderMap.put("remark", "备注");
        try {
            //设置文件MIME类型
            response.setContentType("application/vnd.ms-excel;charset=utf-8");
            //设置Content-Disposition
            response.setHeader("Content-Disposition", "attachment;filename="+ DateUtils.formatDate(new Date(), "yyyyMMddhhmmss")+".xlsx");
            //生成excel表格
            XSSFWorkbook ex = ExcelUtil.generateXlsxWorkbook("价格表", propertyHeaderMap, priceList);
            ex.write(response.getOutputStream());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @RequestMapping("/save")
    public String save(@ModelAttribute Price price, RedirectAttributes redirectAttributes) {
        if (price.getId()==null) {
            priceDao.insert(price);
        } else {
            priceDao.update(price);
        }
        //redirectAttributes.addFlashAttribute("message", "用户【" + price.getName() + "】操作成功");
        return "redirect:/price/list";
    }


    @RequestMapping("delete")
    public String delete(Long id, RedirectAttributes redirectAttributes) {
        priceDao.delete(id);
        redirectAttributes.addFlashAttribute("message", "删除用户成功");
        return "redirect:/price/list";
    }
    
}
