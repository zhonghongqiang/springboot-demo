package com.boya.demo.controlloer;

import com.boya.demo.dao.RegisterDao;
import com.boya.demo.entity.Register;
import com.boya.demo.entity.User;
import com.boya.demo.util.DateStyle;
import com.boya.demo.util.DateTool;
import org.assertj.core.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;


/**
 * 登录控制类
 *
 * @author Boya
 */
@Controller
@RequestMapping(value = "/email")
public class EmailController {
    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private RegisterDao registerDao;

    @Value("${spring.mail.username}")
    private String mailUsername;//发件人邮箱

    @Value("${outTime}")
    private Integer outTime;//随机码有效时间分钟

    @Value("${loginUrl}")
    private String loginUrl;

    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public String test(Model model, User user) {
        return "page/order/email";
    }

    @ResponseBody
    @RequestMapping(value = "/back", method = RequestMethod.POST)
    public HashMap back(Model model, String email) {
        HashMap<String,String> map = new HashMap<String,String>();
        try {
            String password =  (long)(Math.random()*1000000)+"";
            SimpleMailMessage message = new SimpleMailMessage();
            message.setFrom(mailUsername);
            message.setTo(email);

            Register registerOld = registerDao.getByEmail(email);
            if(registerOld!=null){
                Date dateOld = DateTool.StringToDate(registerOld.getJoinTime(),DateStyle.YYYY_MM_DD_HH_MM_SS);
                int minTime = DateTool.getIntervalMin(dateOld, DateUtil.now());
                if(minTime>outTime){
                    //更新随机码
                    message.setSubject("脐橙订购系统随机码(有效时间"+outTime+"分钟)");//邮件主题.
                    message.setText("订购系统地址: \r\n "+loginUrl+" \r\n 随机码: \r\n "+password);//邮件内容.
                    mailSender.send(message);
                    registerOld.setPassword(password);
                    registerDao.update(registerOld);

                    map.put("state","success");
                    map.put("message","登录随机码已成功发送到您的邮箱，请查收！");
                    return map;
                }else{
                    //随机码未过期
                    map.put("state","success");
                    map.put("message","随机码未过期，请查收邮箱！");
                    return map;
                }
            }else{
                //新注册
                message.setSubject("脐橙订购系统随机码(有效时间"+outTime+"分钟)");//邮件主题.
                message.setText("订购系统地址: \r\n "+loginUrl+" \r\n 随机码: \r\n "+password);//邮件内容.
                mailSender.send(message);

                Register register = new Register();
                register.setUserName(email);
                register.setPassword(password);
                registerDao.insert(register);

                map.put("state","success");
                map.put("message","登录随机码已成功发送到您的邮箱，请查收！");
                return map;
            }
        }catch (Exception e){
            e.printStackTrace();
            map.put("state","error");
            map.put("message","非常抱歉，系统出现故障，请联系"+mailUsername);
            return map;
        }
    }

}
