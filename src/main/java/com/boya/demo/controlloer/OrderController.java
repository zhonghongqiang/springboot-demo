package com.boya.demo.controlloer;

import com.boya.demo.dao.OrdersDao;
import com.boya.demo.dao.PriceDao;
import com.boya.demo.entity.Orders;
import com.boya.demo.entity.Price;
import com.boya.demo.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

/**
 * 下单控制类
 *
 * @author Boya
 */
@Controller
@RequestMapping("order")
public class OrderController extends BaseController{

    @Autowired
    OrdersDao ordersDao;
    @Autowired
    PriceDao priceDao;
    
    private static final String path = "page/order/";

    /**
     * 展示訂單頁面
     * @param model
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    public String show(Model model,Long id) {
        model.addAttribute("prices",priceDao.getAll());
        Orders param = new Orders();
        param.setUserId(getCurUser().getId());
        model.addAttribute("orders",ordersDao.findByParam(param));
        param = null;
        if (id!=null){
            param = ordersDao.getById(id);
            //判断是否为可编辑状态,不为1不可编辑
            if (param.getStatus()!=null&&param.getStatus()!=1){
                param = null;
            }
        }
        model.addAttribute("order",param);
        return path+"order";
    }

    /**
     * 保存訂單
     * @param orders
     * @return
     */
    @RequestMapping(value = "add", method = RequestMethod.POST)
    public String save(@ModelAttribute Orders orders,Model model) {
        orders.setStatus(1);
        orders.setUserId(getCurUser().getId());
        for (Price price : priceDao.getAll()) {
            if (price.getQty().equals(orders.getQty())){
                orders.setCost(price.getCost());
                orders.setPrice(price.getAvg());
                break;
            }
        }
        if (orders.getId()!=null){
            ordersDao.update(orders);
        }else {
            ordersDao.insert(orders);
        }
        return "redirect:/order/detail?id="+orders.getId().toString();
    }

    /**
     * 确认付款
     * @param order
     * @return
     */
    @RequestMapping(value = "payment", method = RequestMethod.POST)
    public String payment(Model model,Orders order) {
        order.setStatus(2);
        ordersDao.updateStatus(order);
        return "redirect:/order";
    }

    /**
     * 獲取金额
     * @param price 數量
     * @return
     */
    @RequestMapping(value = "getCostByQty", method = RequestMethod.POST)
    @ResponseBody
    public Price getCostByQty(Price price) {
        if (price.getQty()!=null){
            return priceDao.findByParam(price);
        }
        return price;
    }

    /**
     * 展示訂單详情頁面
     * @param model
     * @return
     */
    @RequestMapping(value = "detail", method = RequestMethod.GET)
    public String showOrderDetail(Model model,Long id) {
        if (id!=null){
            Orders order = ordersDao.getById(id);
            model.addAttribute("order",order);
            return path+"detail";
        }else {
            return "redirect:/order";
        }

    }
}
