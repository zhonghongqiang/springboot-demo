package com.boya.demo.dao;

import com.boya.demo.entity.Register;
import org.springframework.stereotype.Repository;


/**
 * 用户DAO类
 */
@Repository
public interface RegisterDao {
	Register getByEmail(String userName);
	 int insert(Register register);
	 int update(Register register);
}
