package com.boya.demo.dao;

import com.boya.demo.entity.Price;

import java.util.List;

/**
 * Created by zhong on 2016/11/6 0006.
 */
public interface PriceDao {

    public Price getById(Long id);

    public List<Price> getAll();

    public Price findByParam(Price price);

    public int insert(Price Price);

    public int update(Price Price);

    public void delete(Long id);
}
