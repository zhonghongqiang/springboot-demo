package com.boya.demo.dao;

import com.boya.demo.entity.Dic;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * 字典DAO类
 */
@Repository
public interface DicDao {

	 Dic getById(Long id);
	
	 List<Dic> getAll();

	public List<Dic> findByParam(Dic dic);
	
	 int insert(Dic dic);
	
	 int update(Dic dic);
	
	 void delete(Long id);
}
