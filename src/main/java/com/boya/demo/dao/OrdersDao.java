package com.boya.demo.dao;

import com.boya.demo.entity.Orders;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 用户DAO类
 */
@Repository
public interface OrdersDao {
	
	public Orders getById(Long id);

	public List<Orders> getAll();

	public List<Orders> findByParam(Orders orders);

	public int insert(Orders Orders);

	public int update(Orders Orders);

	public int updateStatus(Orders Orders);

	public void delete(Long id);
}
