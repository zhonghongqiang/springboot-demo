/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.20 : Database - school
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`school` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `school`;

/*Table structure for table `dic` */

DROP TABLE IF EXISTS `dic`;

CREATE TABLE `dic` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dicno` int(4) DEFAULT NULL,
  `dickey` varchar(32) DEFAULT NULL,
  `dicvalue` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `dic` */

insert  into `dic`(`id`,`dicno`,`dickey`,`dicvalue`) values (1,1000,'1','待付款'),(2,1000,'2','正在处理'),(3,1000,'3','正在备货'),(4,1000,'4','运输在途'),(5,1000,'5','已收货');

/*Table structure for table `orders` */

DROP TABLE IF EXISTS `orders`;

CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `name` varchar(50) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `leavemessage` varchar(1024) DEFAULT NULL,
  `recconfirmmessage` varchar(1024) DEFAULT NULL,
  `address` varchar(256) DEFAULT NULL,
  `ordertime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deliverydate` timestamp NULL DEFAULT NULL,
  `logistics` varchar(128) DEFAULT NULL,
  `trackingno` varchar(32) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `qty` decimal(8,2) DEFAULT '0.00',
  `price` decimal(8,2) DEFAULT '0.00',
  `cost` decimal(8,2) DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10032 DEFAULT CHARSET=utf8;

/*Data for the table `orders` */

insert  into `orders`(`id`,`userId`,`name`,`mobile`,`leavemessage`,`recconfirmmessage`,`address`,`ordertime`,`deliverydate`,`logistics`,`trackingno`,`status`,`qty`,`price`,`cost`) values (10025,16,'肖英俊','18668097693','又大又甜的',NULL,'中控','2016-11-08 21:14:39','2016-11-08 00:00:00',NULL,NULL,3,100.00,6.25,625.00),(10026,17,'钟宏强','18296127407','留言',NULL,'激励','2016-11-09 19:36:25','2015-12-06 00:00:00','韵达公司','1994022515310256895896589',4,60.00,6.60,396.00),(10027,17,'钟宏强','18296127407','我现在在测试留言','管理员代收货','种宏强','2016-11-09 19:35:06','2016-08-16 00:00:00','韵达','1234567898526985',5,60.00,6.60,396.00),(10028,16,'你现在在干嘛呢','18296127407','',NULL,'我现在在测试','2016-11-09 21:03:52','2016-12-12 00:00:00','韵达公司','12345678989456789',4,10.00,6.90,69.00),(10030,17,'分配给我的全部','15554101806','',NULL,'','2016-11-09 19:53:29','2015-12-31 00:00:00',NULL,NULL,3,40.00,6.70,268.00),(10031,19,'王子珍','15869031707','ok','','杭州市滨江区西兴街道铁岭花园','2016-11-09 20:20:39','2015-12-06 00:00:00','韵达公司','25698584578455445145541',5,100.00,6.25,625.00);

/*Table structure for table `price` */

DROP TABLE IF EXISTS `price`;

CREATE TABLE `price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qty` decimal(8,2) NOT NULL DEFAULT '0.00',
  `cost` decimal(8,2) NOT NULL DEFAULT '0.00',
  `remark` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

/*Data for the table `price` */

insert  into `price`(`id`,`qty`,`cost`,`remark`) values (14,10.00,69.00,'1箱10斤装'),(16,20.00,136.00,'1箱20斤装'),(18,30.00,204.00,'1箱20斤装+1箱10斤装'),(19,40.00,268.00,'2箱20斤装'),(20,50.00,335.00,'2箱20斤装+1箱10斤装'),(21,60.00,396.00,'3箱20斤装'),(22,70.00,462.00,'3箱20斤装+1箱10斤装'),(23,80.00,520.00,'4箱20斤装'),(24,90.00,585.00,'4箱20斤装+1箱10斤装'),(25,100.00,625.00,'5箱20斤装');

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `role` */

insert  into `role`(`id`,`name`) values (2,'用户');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`name`,`password`,`age`,`role_id`) values (9,'admin@qq.com','admin',30,1),(37,'boya','boya',23,2),(38,'test2','test',33,1);

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `Id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `userName` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `joinTime` datetime DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='客户注册表';

/*Data for the table `users` */

insert  into `users`(`Id`,`password`,`name`,`address`,`userName`,`phone`,`joinTime`,`age`,`role_id`) values (16,'395575','189802556@qq.com',NULL,NULL,NULL,'2016-11-09 21:48:54',NULL,NULL),(17,'960514','zhong.zhq@qq.com',NULL,NULL,NULL,'2016-11-09 19:16:20',NULL,NULL),(18,'787911','clear_windy@yeah.net',NULL,NULL,NULL,'2016-11-08 22:54:11',NULL,NULL),(19,'204303','515739380@qq.com',NULL,NULL,NULL,'2016-11-09 20:53:06',NULL,NULL);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
